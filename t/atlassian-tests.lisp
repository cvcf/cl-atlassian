(defpackage #:com.cameronchaparro.atlassian/tests
  (:nicknames #:cl-atlassian/tests #:atlassian/tests)
  (:use #:cl #:fiveam #:cl-atlassian #:base64 #:str)
  (:export #:run-all-tests))

(in-package #:com.cameronchaparro.atlassian/tests)

(def-suite atlassian-tests)
(in-suite atlassian-tests)
