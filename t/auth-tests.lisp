(in-package #:com.cameronchaparro.atlassian/tests)

(def-suite auth-tests)
(in-suite auth-tests)

(defparameter *username* "username")
(defparameter *password* "password")

(test base64-encoding
  (is (string= (encode-username-password *username* *password*)
               (string-to-base64-string (concat *username* ":" *password*)))))

(test basic-auth-header
  (let ((header (get-authentication-header :username *username*
                                           :password *password*)))
    (is (first header) :authorization)
    (destructuring-bind (type token) (split " " (rest header))
      (is (string= type (capitalize *auth-type*)))
      (is (string= token (encode-username-password *username* *password*))))))
