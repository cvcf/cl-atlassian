(defsystem #:cl-atlassian
  :description "A library for interacting with an Atlassian service."
  :version "0.0.1"
  :author "Cameron Chaparro <cameron@cameronchaparro.com>"
  :license "GPL3"
  :depends-on (#:cl-base64 #:cl-json #:dexador #:str)
  :serial t
  :pathname "src/"
  :components ((:file "auth")
               (:file "package"))
  :in-order-to ((test-op (test-op #:cl-atlassian/tests))))

(defsystem #:cl-atlassian/tests
  :depends-on (#:fiveam #:cl-atlassian #:cl-base64 #:str)
  :serial t
  :pathname "t/"
  :components ((:file "atlassian-tests")
               (:file "auth-tests"))
  :perform (test-op (o c) (uiop:symbol-call :atlassian/tests :run-all-tests)))
