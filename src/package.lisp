(defpackage #:com.cameronchaparro.atlassian
  (:nicknames #:cl-atlassian #:atlassian)
  (:use #:cl)

  ;; Authentication
  (:use #:com.cameronchaparro.atlassian/auth)
  (:export #:*auth-type*
           #:get-authentication-header
           #:encode-username-password))
