(defpackage #:com.cameronchaparro.atlassian/auth
  (:nicknames #:cl-atlassian/auth #:atlassian/auth)
  (:use #:cl)
  (:import-from #:base64 #:string-to-base64-string)
  (:import-from #:str #:capitalize #:concat)
  (:export #:*auth-type*
           #:get-authentication-header
           #:encode-username-password))

(in-package #:com.cameronchaparro.atlassian/auth)

(defparameter *auth-type* :basic
  "The type of authentication to use with the service. Currently, only basic authentication is supported.")

(defun get-authentication-header (&key username password &allow-other-keys)
  "Return an HTTP header used for authentication."
  (let ((token (encode-username-password username password)))
    (cons :authorization (concat (capitalize *auth-type*) " " token))))

(defun encode-username-password (username password)
  "Base64 encode a username/password pair for inclusion in an HTTP header."
  (string-to-base64-string (concat username ":" password)))
