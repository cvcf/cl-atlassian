(require 'password-store)

(let ((token (getenv "ATLASSIAN_TOKEN")))
  (unless (and token (not (string-empty-p token)))
    (password-store-get "atlassian/launch-plan-generator-token"
                        (lambda (token) (setenv "ATLASSIAN_TOKEN" token)))))

(provide 'cl-atlassian-token)
